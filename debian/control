Source: lomiri-system-settings
Section: misc
Priority: optional
Maintainer: UBports developers <devs@ubports.com>
Build-Depends:
 cmake,
 cmake-extras,
 dbus-test-runner,
 debhelper-compat (= 12),
 dh-migrations:all | hello,
 dh-python,
 dpkg-dev <!nocheck>,
 gdb <!nocheck>,
 gir1.2-glib-2.0 <!nocheck>,
 intltool,
 libaccountsservice-dev,
 libandroid-properties-dev [amd64 arm64 armhf i386] | hello,
 libapt-pkg-dev,
 libclick-0.4-dev,
 libdeviceinfo-dev,
 libevdev-dev,
 libgeonames-dev,
 libglib2.0-dev (>= 2.37.92),
 libgnome-desktop-3-dev,
 libgsettings-qt-dev,
 libicu-dev,
 libpolkit-agent-1-dev,
 libqmenumodel-dev,
 libqt5sql5-sqlite <!nocheck>,
 libqtdbusmock1-dev (>= 0.2+14.04.20140724) <!nocheck>,
 libqtdbustest1-dev <!nocheck>,
 libsystemd-dev,
 libtrust-store-dev,
 libudev-dev,
 libupower-glib-dev,
 pkgconf,
 python3-all:any,
 python3-dbus (>= 0.30.2) <!nocheck>,
 python3-flake8 (>= 2.2.2-1ubuntu4) | python3-flake8:native <!nocheck>,
 python3-gi <!nocheck>,
 python3-pep8 <!nocheck>,
 python3-setuptools,
 python3:any <!nocheck>,
 qml-module-lomiri-components <!nocheck>,
 qml-module-lomiri-settings-components <!nocheck>,
 qml-module-lomiri-settings-fingerprint <!nocheck>,
 qml-module-lomiri-settings-menus <!nocheck>,
 qml-module-lomiri-settings-vpn <!nocheck>,
 qml-module-ofono (>=0.117~) <!nocheck>,
 qml-module-qtcontacts,
 qml-module-qtquick-layouts,
 qml-module-qtquick2 <!nocheck>,
 qml-module-qtsysteminfo (>= 5.0~),
 qml-module-qttest <!nocheck>,
 qtbase5-dev,
 qtbase5-private-dev <!nocheck>,
 qtdeclarative5-dev,
 qtdeclarative5-dev-tools,
 suru-icon-theme,
 xvfb <!nocheck>,
 xauth <!nocheck>,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://gitlab.com/ubports/core/lomiri-system-settings
Vcs-Git: https://gitlab.com/ubports/core/lomiri-system-settings.git
Vcs-Browser: https://gitlab.com/ubports/core/lomiri-system-settings

Package: lomiri-system-settings
Architecture: any
Depends:
 accountsservice,
 ${accountsservice-ubuntu-schemas},
 bluez (>= 5.23),
 click | ubuntu-snappy-cli,
 gir1.2-glib-2.0,
 gsettings-desktop-schemas,
# TODO: migrate to Ayatana indicators
# indicator-bluetooth (>> 0.0.6+13.10.20131010),
# indicator-datetime,
# indicator-power (>= 12.10.6+15.04.20150130),
 liblomirisystemsettings1 (= ${binary:Version}),
 lomiri-indicator-network,
 lomiri-keyboard-data,
 lomiri-schemas,
 python3,
 python3-dbus,
 python3-gi,
 qmenumodel-qml,
 qml-module-gsettings1.0,
 qml-module-ofono (>=0.117~),
 qml-module-lomiri-components,
 qml-module-lomiri-components-extras,
 qml-module-lomiri-connectivity (>= 0.7.1),
 qml-module-lomiri-content,
 qml-module-lomiri-settings-components,
 qml-module-lomiri-settings-fingerprint,
 qml-module-lomiri-settings-menus,
 qml-module-lomiri-settings-vpn,
 qml-module-ofono (>=0.117~),
 qml-module-qt-labs-folderlistmodel,
 qml-module-qtmultimedia | qml-module-qtmultimedia-gles,
 qml-module-qtsysteminfo,
 suru-icon-theme (>= 14.04+15.04.20150813~),
 lomiri-wallpapers,
 upower,
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 aethercast,
 dbus-property-service,
 system-image-dbus (>= 3.1),
 urfkill,
Recommends:
 lomiri-sounds,
 lomiri-system-settings-online-accounts,
 repowerd,
Breaks:
 lomiri-system-settings-security-privacy,
Replaces:
 lomiri-system-settings-security-privacy,
Provides:
 lomiri-system-settings-security-privacy,
Description: System Settings application for Lomiri
 Lomiri-system-settings is the System Settings application used in Lomiri
 operating environment. it's designed for phones, tablets and convergent
 devices.

Package: liblomirisystemsettings1
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: System Settings application for Lomiri - plug-in library
 Lomiri-system-settings is the System Settings application used in Lomiri
 operating environment. it's designed for phones, tablets and convergent
 devices.
 .
 This package contains the library used by settings plugins.

Package: liblomirisystemsettings-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 liblomirisystemsettings1 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: System Settings application for Lomiri - plug-in development files
 Lomiri-system-settings is the System Settings application used in Lomiri
 operating environment. it's designed for phones, tablets and convergent
 devices.
 .
 This package contains the plug-in library's development files.

Package: liblomirisystemsettingsprivate0.0
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 liblomirisystemsettingsprivate0,
Replaces:
 liblomirisystemsettingsprivate0,
Provides:
 liblomirisystemsettingsprivate0,
Description: System Settings application for Lomiri - private library
 Lomiri-system-settings is the System Settings application used in Lomiri
 operating environment. it's designed for phones, tablets and convergent
 devices.
 .
 This package contains the private library used by some settings plugins.

Package: liblomirisystemsettingsprivate-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 liblomirisystemsettingsprivate0.0 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: System Settings application for Lomiri - private development files
 Lomiri-system-settings is the System Settings application used in Lomiri
 operating environment. it's designed for phones, tablets and convergent
 devices.
 .
 This package contains the private library's development files.

Package: lomiri-system-settings-autopilot
Architecture: all
Depends:
 dpkg-dev,
 gir1.2-upowerglib-1.0,
 libautopilot-qt,
 python3-autopilot,
 python3-dateutil,
 python3-dbusmock (>= 0.14),
 python3-evdev,
 lomiri-system-settings,
 lomiri-system-settings-phone,
 lomiri-system-settings-cellular,
 lomiri-ui-toolkit-autopilot,
 ${misc:Depends},
 ${python3:Depends},
Description: System Settings application for Lomiri - Autopilot tests
 Lomiri-system-settings is the System Settings application used in Lomiri
 desktop environment. it's designed for phones, tablets and convergent
 devices.
 .
 This package contains the autopilot tests for lomiri-system-settings.
